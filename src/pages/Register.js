
import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Register() {

  const { user, setUser } = useContext(UserContext)
  //create state hooks to store the values to input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email,setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  //create a state to determine whether the submit button is enabled or not

  const [isActive, setIsActive] = useState('');

  console.log(firstName)
  console.log(lastName)
  console.log(email)
  console.log(mobileNo)
  console.log(password1)
  console.log(password2)

  function registerUser(e){

const data = {
"firstName": firstName,
"lastName": lastName,
"email": email,
"password": password1,
"mobileNo": mobileNo
}
const requestOptions = {
method: "POST",
        headers: {"Content-Type": "application/json"},
   
body: JSON.stringify(data)
};
fetch(`${process.env.REACT_APP_API_URL}/users/register`, requestOptions)
.then(response => response.json())
.then(Data => {
  console.log(Data)
  alert(Data.message)


})
  }



  useEffect(()=>{

    if((firstName !=="" && lastName !== "" && mobileNo.length === 11 && email !=="" && password1 !=="" && password2 !=="")&&(password1===password2)){
      setIsActive(true)
    }else{
      setIsActive(false)

    }

  },[firstName,lastName,email,mobileNo,password1,password2])

  return (

    (user.id !== null)
    ?
    <Navigate to="/products"/>
    :
    <Form onSubmit={(e)=>registerUser(e)}>

      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}
        required />
      </Form.Group>

       <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
          required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required/>
        <Form.Text className="text-muted">
           We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        type="number"
        placeholder="Enter Mobile Number"
        value={mobileNo}
        onChange={e=>setMobileNo(e.target.value)}
        required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
        required />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
        required/>
      </Form.Group>


   {isActive ?
        <Button variant="dark" type="submit" id="submitBtn">
          Submit
        </Button>
        :
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      }
    </Form>
  );
}
