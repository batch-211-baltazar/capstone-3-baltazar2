import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Advertisement from '../components/Advertisement'

export default function Home(){

	const data = {
        title: "Lab Collection",
        content: '"Never too old for a toy!"',
        destination: "/products",
        label: "Browse All Products"
    }

	return(
		<>
			<Banner bannerProp={data}/>
			<Advertisement/>
			<Highlights/>
			
		</>
	)
}
