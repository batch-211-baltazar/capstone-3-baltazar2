import Banner from '../components/Banner.js'

export default function Error() {

    const data = {
        title: "Oops! Page not Found!",
        content: "The page you are looking cannot be found.",
        destination: "/",
        label: "Return to Home Page"
    }
    return (
    <Banner bannerProp = {data}/>
    )
}


// import React from 'react'

// const Error = () => {
//     return (
//         <div id="wrapper">
//             <img src="https://i.imgur.com/qIufhof.png" />
//             <div id="info">
//                 <h3>This page could not be found</h3>
//             </div>
//         </div >
//     )
// }

// export default Error