import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
  const [email,setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState('');

console.log(email);
console.log(password);

const { user, setUser } = useContext(UserContext);

function authenticate(e){

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);
      if(typeof data.access !== "undefined"){
        localStorage.setItem('token',data.access)
        localStorage.setItem('userId', data.userId);
        retrieveUserDetails(data.access)

        Swal.fire({
          title: "Login Successful!",
          icon: "success",
          text: (`Welcome to Lab Collection!`)
        });
      }else{
        Swal.fire({
          title: "Authentication Failed!",
          icon: "error",
          text: "Email address and password did not match, try again!"
        })
      }
    })

    const retrieveUserDetails = (token) =>{

      fetch(`${process.env.REACT_APP_API_URL}/users/UserDetails`,{
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      })
    }


    setEmail("");
    setPassword("");

  }

useEffect(()=>{

    if(email !=="" && password !==""){
      setIsActive(true)
    }else{
      setIsActive(false)

    }

  },[email,password])

  return (
    //Conditional Rendering
    // LOGIC - if there is a user logged-in in the web application, endpoint "/login" should not be accessible. The user should be navigated to courses tab instead.
    (user.id !== null)
    ?
    <Navigate to="/products"/>
    // ELSE - if the localStorage is empty, the user is allowed to access the login page.
    :
    <Form onSubmit={(e)=>authenticate(e)}>
    <Form.Label>Login</Form.Label>
      <Form.Group className="mb-3" controlId="userEmail2">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required/>
        <Form.Text className="text-muted">
           We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>


      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password}
        onChange={e=>setPassword(e.target.value)}
        required />
      </Form.Group>



   {isActive ?
        <Button variant="dark" type="submit" id="submitBtn">
          Login
        </Button>
        :
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Login
        </Button>
      }
    </Form>

  );
}

     
    
