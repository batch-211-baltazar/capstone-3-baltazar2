import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products(){

	const [products, setProducts] =useState([])

	const {user} = useContext(UserContext);
	// console.log(user);

	useEffect(()=>{
		// console.log(process.env.REACT_APP_API_URL)
	// fetch(`${process.env.REACT_APP_API_URL}/products`)
	fetch(`${process.env.REACT_APP_API_URL}/products/active`)
	.then(res=>{

		// console.log(res)
		return(res.json())
		

	})
	.then(data=>{
		// console.log(data)
		
	const productArr = (data.map(product => {
		return (
			<ProductCard productProp={product} key={product._id}/>
			)
		}))
		setProducts(productArr)
	})
	},[products.length])

	//***add a way to /admin
	return(
		(user.isAdmin)?
		<Navigate to="/Admin"/>
		:
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}
