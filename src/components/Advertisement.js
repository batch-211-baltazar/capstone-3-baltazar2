import {Row, Col, Card} from 'react-bootstrap';
import React from 'react';
import ads from './ads.gif';


export default function Advertisement(){
  return(
  <Row className = "mt-3 mb-3">
    <Col xs={12} md={12}>
      <Card className="Advertisementcard">
        <Card.Body>
        <img src={ads} alt="..." class="w-100" />
          <Card.Title>Get yours now!</Card.Title>
          <Card.Text>
          Sign up to Lab Collection's Box and get monthly surprises. Includes anime figures from Banpresto, Qposket, and Funkopop. Other inclusions are plushies and accessories.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    );
}

