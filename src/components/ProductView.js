import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const [userId, setUserId] = useState("");
	// const [orderId, setOrderId] = useState("");

	const order = (productId) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/order`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: localStorage.getItem("userId"),
				quantity: quantity,
				price: price
			})
		})
		.then(res=>res.json())
		.then(data=>{
			
			if(data===true){

				Swal.fire({
				  title: "Order Successful!",
				  icon: "success",
				  text: "Thank you for your purchase!"
				});
				navigate("/products")

			}else{

				Swal.fire({
				  title: "Something went wrong!",
				  icon: "error",
				  text: "User login is required!"
				});

			}

		})
	}

	useEffect(()=>{
		console.log(productId);
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
				
		})


	},[productId])


	return(
		<Container className="mt-5">
		  <Row>
		     <Col lg={{span:6, offset:3}}>
				<Card>
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
							<form>
  <div class="form-group">
    <label for="orderQuantity">Quantity</label>
    <select class="quantity-control" id="orderQuantity" value={quantity}
        onChange={e=>setQuantity(e.target.value)}>
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
      <option>6</option>
      <option>7</option>
      <option>8</option>
      <option>9</option>
      <option>10</option>
    </select>
  </div>
</form>
						{
							(user.id!==null)?
							<Button variant="dark" onClick={()=>order(productId)}>Order</Button>
							:
							<Link className="btn btn-dark" to="/login">Log in to Order</Link>
						}
					</Card.Body>

				</Card>
		     </Col>
		  </Row>
		  		

		</Container>

	)

}

