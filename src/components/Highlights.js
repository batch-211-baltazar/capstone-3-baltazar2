import {Row, Col, Card} from 'react-bootstrap';
import anime from './anime.jpg';
import marvel from './marvel.jpg';
import off from './20off.gif';

export default function Highlights(){
  return(
    <Row className = "mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
          <img src={anime} alt="" width= "350"/>
          <Card.Title>Anime Lovers' Corner</Card.Title>
          <Card.Text>
          Collect figures from your favorite Anime!
          FunkoPop, Banpresto, QPocket, and other merch are available here!
          Coming Soon: Manga and Exclusive figures
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
         <img src={marvel} alt="" width= "350"/>
          <Card.Title>Marvel Collections</Card.Title>
          <Card.Text>
          Get your desired collection of Marvel Heroes here. The Avengers, the X-men, the Fantastic Four, and the Guardians of the Galaxy are all available.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3">
        <Card.Body>
         <img src={off} alt="" width= "350"/>
          <Card.Title>Get 20% off on your First Order</Card.Title>
          <Card.Text>
          Offer only available on qualifying products and cannot be combined with other discounts or promotions. Available only up until December of 2022.
         </Card.Text>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    );
}

