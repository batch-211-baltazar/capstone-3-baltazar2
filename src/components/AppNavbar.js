import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
import { useContext } from "react";
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import logo from './mainlogo.gif'; 


export default function AppNavbar() {

const { user } = useContext(UserContext);

return(

<Navbar bg="dark" variant="dark" expand="lg">
        <Container>
        <img src={logo} alt="Logo" width="50"/>
          <Navbar.Brand as={Link} to="/">Lab Collection</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/products">Browse All Products</Nav.Link>
              </Nav>

                            <Nav>
              {/*we use the id of the user to conditionaly render the login, register and logout buttons*/}
              {(user.id !== null) ?
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register">Register</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>


)
}