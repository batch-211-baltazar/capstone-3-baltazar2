import Home from './pages/Home';
import Orders from './pages/Orders';
import Products from './pages/Products';
import Cart from './pages/Cart';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Login from './pages/Login';
import Error from './pages/Error';
import {Container} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar'
import Dash from './pages/Dash';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import ProductView from './components/ProductView';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';


function App(){

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

 const unsetUser = () =>  {
  localStorage.clear();
 }

 // useEffect(() => {

 // },[])

 useEffect(()=>{
  fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
    headers:{
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res=>res.json())
  .then(data=>{
    console.log(data);

    if(typeof data._id !== "undefined"){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      });
    }else{
      setUser({
        id: null,
        isAdmin: null
      })
    }
  })
 },
 []
 );


  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
       <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/orders" element={<Orders/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/cart" element={<Cart/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="*" element={<Error/>}/>
            <Route path="/Admin" element={<Dash/>}/>
            <Route path="/AddProduct" element={<AddProduct/>}/>
            <Route path="/EditProduct/:productId" element={<EditProduct/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
          </Routes>
        </Container>
    </Router>

    </UserProvider>
 
  );
}

export default App;
